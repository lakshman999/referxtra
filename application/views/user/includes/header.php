<!--    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

      <a class="navbar-brand mr-1" href="index.html">User Panel</a>

      <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
      </button>

    </nav> -->


<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Search Job</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
   <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="../../assests/css/style.css" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
    .top_search {
      width:100%;
      background: url("../../assests/images/search-top.png");
      height: 158px;
    }

    @media (min-width: 992px) {
      .container {
        width: 100%;
      }
    }
  </style>
</head>

<body>
    <div class="col-md-12 col-sm-12 col-xs-12 lading_b" style="padding: 0px;">
 <nav class="navbar navbar-default navba_r">
   <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed mo_c" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
<!--            <a class="navbar-brand" href="#"><b>Referxtra</b></a>-->
        <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="../../assests/images/logo1.png" width="180" alt="logo" title="logo"></a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
            <?php
              if(!empty($this->session->userdata('uid'))) {
            ?>
              <a href="../navbar-static-top/" class="dropdown-toggle" style="color: #fff !important;" data-toggle="dropdown"><i class="fa fa-sign-in"></i> <?php echo $this->session->userdata('fname').' '.$this->session->userdata('lname'); ?></a>
                <ul class="dropdown-menu me_nu">
                  <li><a href="javascript: changeDashboard('2')" id="hirerDashboard">Hirer Dashboard</a></li>
                  <li><a href="javascript: changeDashboard('3')" id="referrerDashboard">Referrer Dashboard</a></li>
                  <li><a href="javascript: changeDashboard('4')" id="applicantDashboard">Applicant Dashboard</a></li>
                  <li><a href="#">Profile</a></li>
                  <li><a href="<?php echo site_url('user/Login/logout'); ?>">Logout</a></li>
                </ul>
              
                <?php } else { ?>
                  <a class="card-footer clearfix small z-1" style="color: #fff !important;" href="<?php echo site_url('user/login'); ?>">Login</a>
                <?php } ?>
              </li>
            </ul>

          </div><!--/.nav-collapse -->
      </div>
      </nav>
  </div>

<script>

  function changeDashboard(val) {
    $('#user_type').val(val).trigger('change');
  }

</script>