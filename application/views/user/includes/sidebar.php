<?php

  if ($this->uri->segment(2) == 'dashboard' || $this->uri->segment(3) == 'myjobs' || $this->uri->segment(3) == 'createjob')
  {
?>

    <div class="col-md-3 col-sm-6 col-xs-12 left_job">
         <div class="col-md-12 left_s" style="padding: 0px;">
          <div class="col-md-12 header_l" align="center">
            <img src="../../../assests/images/no_user_image.png" width="100" />
            <div style="clear: both;"></div>
            <div align="center">
              <strong><?php echo $this->session->userdata('fname').' '.$this->session->userdata('lname'); ?></strong>
            </div>
            <div style="clear: both;"></div>
            <div>
            <?php
              if($this->input->post('user_type') == '2' || $this->session->userdata('user_type') == 2) {
                $hirerSelected = " selected='selected'";
                $referrerSelected = "";
                $applicantSelected = "";
              } elseif($this->input->post('user_type') == '3' || $this->session->userdata('user_type') == 3) {
                $hirerSelected = "";
                $referrerSelected = " selected='selected'";
                $applicantSelected = "";
              } else {
                $hirerSelected = "";
                $referrerSelected = "";
                $applicantSelected = " selected='selected'";
              }

            ?>
              <form action="./myjobs" method="POST">
                <div class="styled-select">
                   <select name="user_type" id="user_type">
                     <option value="2" <?php echo $hirerSelected; ?>>Hirer</option>
                     <option value="3" <?php echo $referrerSelected; ?>>Referrer</option>
                     <option value="4" <?php echo $applicantSelected; ?>>Applicant</option>
                   </select>
                  <span class="fa fa-sort-desc"></span>
                </div>
              </form>
            </div>
        </div>
         
         <div class="col-md-12 body_l">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

            <?php if($this->input->post('user_type') == '2' || $this->session->userdata('user_type') == 2) { ?>

              <div class="col-md-12 body_l">
                <ul class="list_inli">
                  <li><a href="./myjobs"><i class="fa fa-hand-o-right han_fa"></i> &nbsp;My Jobs</a></li>
                  <li><a href="./createjob"><i class="fa fa-hand-o-right han_fa"></i> &nbsp;Create Job</a></li>
                  <li><a href="#"><i class="fa fa-hand-o-right han_fa"></i> &nbsp;My Applicants</a></li>
                  <li><a href="#"><i class="fa fa-hand-o-right han_fa"></i> &nbsp;My Transactions</a></li>
                </ul>
              </div>
            <?php } elseif($this->input->post('user_type') == '3' || $this->session->userdata('user_type') == 3) { ?>

              <div class="col-md-12 body_l">
                <ul class="list_inli">
                  <li><a href="#"><i class="fa fa-hand-o-right han_fa"></i> &nbsp;My Referrels</a></li>
                  <li><a href="#"><i class="fa fa-hand-o-right han_fa"></i> &nbsp;My Transactions</a></li>
                </ul>
              </div>
            <?php } else { ?>

              <div class="col-md-12 body_l">
                <ul class="list_inli">
                  <li><a href="#"><i class="fa fa-hand-o-right han_fa"></i> &nbsp;My Jobs</a></li>
                  <li><a href="#"><i class="fa fa-hand-o-right han_fa"></i> &nbsp;My Transactions</a></li>
                </ul>
              </div>
            <?php } ?>
            </div>
          </div>
        </div>
    </div>


<?php
  }
  else
  {
?>
<div class="col-md-3 col-sm-6 col-xs-12 left_job">
         <div class="col-md-12 left_s" style="padding: 0px;">
          <div class="col-md-12 header_l">
            <h5 class="fil_5">Filter By</h5>
        </div>
         
         <div class="col-md-12 body_l">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
            <i class="more-less glyphicon glyphicon-plus"></i>
            Skill : 
          </a>
        </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
           <form action="#">
            <p>
            <input type="checkbox" id="test1" checked>
            <label for="test1">Apple</label>
            </p>
            <p>
            <input type="checkbox" id="test2">
            <label for="test2">Peach</label>
            </p>
            <p>
            <input type="checkbox" id="test3">
            <label for="test3">Orange</label>
            </p>
             <p>
            <input type="checkbox" id="test4">
            <label for="test4">Apple</label>
            </p>
            <p>
            <input type="checkbox" id="test5">
            <label for="test5">Peach</label>
            </p>
            <p>
            <input type="checkbox" id="test6">
            <label for="test6">Orange</label>
            </p>
          </form>
        </div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingTwo">
        <h4 class="panel-title">
          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            <i class="more-less glyphicon glyphicon-plus"></i>
            Location :
          </a>
        </h4>
      </div>
      <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
        <div class="panel-body">
           <form action="#">
            <p>
            <input type="checkbox" id="test13">
            <label for="test13">Apple</label>
            </p>
            <p>
            <input type="checkbox" id="test14">
            <label for="test14">Peach</label>
            </p>
            <p>
            <input type="checkbox" id="test15" checked>
            <label for="test15">Orange</label>
            </p>
             <p>
            <input type="checkbox" id="test16">
            <label for="test16">Apple</label>
            </p>
            <p>
            <input type="checkbox" id="test17">
            <label for="test17">Peach</label>
            </p>
            <p>
            <input type="checkbox" id="test18">
            <label for="test18">Orange</label>
            </p>
          </form>
        </div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingThree">
        <h4 class="panel-title">
          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
            <i class="more-less glyphicon glyphicon-plus"></i>
            Job Roll :
          </a>
        </h4>
      </div>
      <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
        <div class="panel-body">
           <form action="#">
            <p>
            <input type="checkbox" id="test7">
            <label for="test7">Apple</label>
            </p>
            <p>
            <input type="checkbox" id="test8">
            <label for="test8">Peach</label>
            </p>
            <p>
            <input type="checkbox" id="test9">
            <label for="test9">Orange</label>
            </p>
             <p>
            <input type="checkbox" id="test10">
            <label for="test10">Apple</label>
            </p>
            <p>
            <input type="checkbox" id="test11">
            <label for="test11">Peach</label>
            </p>
            <p>
            <input type="checkbox" id="test12">
            <label for="test12">Orange</label>
            </p>
          </form>
        </div>
      </div>
    </div>

  </div><!-- panel-group -->
  
         </div>
       </div>
       </div>
<?php } ?>

<script>
  $(function() {
      $('#user_type').change(function() {
          this.form.submit();
      });
  });
</script>