<!DOCTYPE html>
<html lang="en">

<head>
<title>Search Jobs</title>
<!-- Bootstrap core CSS-->
<?php echo link_tag('assests/vendor/bootstrap/css/bootstrap.min.css'); ?>
<!-- Custom fonts for this template-->
<?php echo link_tag('assests/vendor/fontawesome-free/css/all.min.css'); ?>
<!-- Page level plugin CSS-->
<?php echo link_tag('assests/vendor/datatables/dataTables.bootstrap4.css'); ?>
<!-- Custom styles for this template-->
<?php echo link_tag('assests/css/sb-admin.css'); ?>

  </head>

  <body id="page-top">

   <?php include APPPATH.'views/user/includes/header.php';?>

    <div id="wrapper">

      <!-- Sidebar -->
  <?php include APPPATH.'views/user/includes/sidebar.php';?>

      <div id="content-wrapper">

        <div class="container-fluid">

        <div>
        <?php
        //echo '<pre>'; print_r($searchJobs); 
          foreach ($searchJobs as $result) {
        ?>
          <div class="col-md-12 ftco-animate">

            <div class="job-post-item bg-white p-4 d-block d-md-flex align-items-center">

              <div class="mb-4 mb-md-0 mr-5" style="width: 50%;">
                <div class="job-post-item-header d-flex align-items-center">
                  <h2 class="mr-3 text-black h3"><?php echo $result['title']; ?></h2>
                  <div class="badge-wrap">
                   <span class="bg-primary text-white badge py-2 px-3"><?php echo $result['job_type']; ?></span>
                  </div>
                </div>
                <div class="job-post-item-body d-block d-md-flex">
                  <div class="mr-3"><span class="icon-layers"></span> <a href="#"><?php echo $result['company_name']; ?></a></div>
                  <div><span class="icon-my_location"></span> <span><?php echo $result['location']; ?></span></div>
                </div>
                <div class="job-post-item-body d-block d-md-flex">
                  <span><b>Exp. <?php echo $result['experience_from'].' - '.$result['experience_to'].' Yrs'; ?></b></span>
                </div>
                <div class="job-post-item-body d-block d-md-flex">
                  <div><span class="icon-my_summary"></span> <span>
                    <?php 
                      if(strlen($result['summary']) > 500)
                        echo substr($result['summary'], 0, 500).' ...';
                      else {
                         echo $result['summary'];
                       } ?>   
                  </span></div>
                </div>
              </div>

              <div class="mb-4 mb-md-0 mr-5">
                <div class="job-post-item-header d-flex align-items-center">
                  <h2 class="mr-3 text-black h3">Referral Rewards</h2>
                </div>
                
                <div class="job-post-item-body d-block d-md-flex">Application</div>
                <div class="job-post-item-body d-block d-md-flex">
                    <b><?php echo $result['accepted_application_rewards']; ?></b>
                </div>

                <div class="job-post-item-body d-block d-md-flex">Interview</div>
                <div class="job-post-item-body d-block d-md-flex">
                    <b><?php echo $result['schedule_interview_rewards']; ?></b>
                </div>

                <div class="job-post-item-body d-block d-md-flex">Hire</div>
                <div class="job-post-item-body d-block d-md-flex">
                    <b><?php echo $result['offerletter_release_rewards']; ?></b>
                </div>

              </div>
              
              </div>
              </div>

              <div class="ml-auto d-flex">
                <a href="index.php?q=apply&job=<?php echo $result['id'];?>&view=personalinfo" class="btn btn-primary py-2 mr-1">Apply Job</a>
              
              </div>
            
          <?php } ?>

        </div>

        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
     <?php include APPPATH.'views/user/includes/footer.php';?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>
  <!-- Bootstrap core JavaScript-->
    <script src="<?php echo base_url('assests/vendor/jquery/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assests/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <!-- Core plugin JavaScript-->
    <script src="<?php echo base_url('assests/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>
    <!-- Custom scripts for all pages-->
    <script src="<?php echo base_url('assests/js/sb-admin.min.js '); ?>"></script>

  </body>

</html>
