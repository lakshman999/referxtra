<?php include APPPATH.'views/user/includes/header.php';?>

<style>
    .top_search {
      width:100%;
      background: url("../../assests/images/create_job.png");
      height: 158px;
    }
</style>

<div class="col-md-12 col-sm-12 col-xs-12 top_search">
    <div class="container">
        <h4 class="text-center h4_p">Create Job</h4>
    </div>
</div>

<div class="">
    <div class="col-md-12 col-sm-12 col-xs-12 full_box" style="margin-top: 25px;">
        <?php include APPPATH.'views/user/includes/sidebar.php';?>

        <div class="col-md-9 col-sm-6 col-xs-12 right_job">
            <div class="col-md-12 body_1" style="padding: 0px;">
                  <ul class="nav nav-tabs pull-right" role="tablist">
					<li class="nav-item active">
						<a class="nav-link active" data-toggle="tab" href="#tab1">Job Details</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#tab2">Job Settings</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#tab3">Job Preview</a>
					</li>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<div id="tab1" class="tab-pane active">
						<br>
						<div class="col-md-12 col-sm-12 col-xs-12 creat_j">
						  <div class="col-md-6 left_min">
							 <div class="col-md-12 mil_e" style="padding: 0px;">
							   <form class="form-horizontal" action="" method="">
								  <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Job ID </label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;">
									    <input type="text" class="form-control" id="jobid" name="jobid" placeholder="Job ID">
									  </div>
								  </div> 
								   
								   <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Job Title</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;">
									    <input type="text" class="form-control" id="job_title" name="job_title" placeholder="Job Title">
									  </div>
								  </div> 
								   
								   <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Location</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;">
									    <select class="selectpicker form-control" name="location" id="location">
											<option value="">Select Location</option>
	                                      	<?php 
	                                        	foreach ($locations as $row) {
		                                        	echo '<option value='.$row['id'].'>'.$row['location'].'</option>';
		                                        }
	                                      	?>
									  	</select>
									  </div>
								  </div> 
								   
								   <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Job Type</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;">
									    <select class="selectpicker form-control" name="job_type" id="job_type">
											<option>Job Type</option>
											<?php 
		                                        foreach ($jobtypes as $row) {
		                                          echo '<option value='.$row['id'].'>'.$row['name'].'</option>';
		                                        }
	                                      	?>
									  	</select>
									  </div>
								  </div> 
								   
								    <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Summary</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;">
									 <textarea class="wid" rows="5" name="summary" id="summary" placeholder="Summary"></textarea>
									  </div>
								  </div>
								   
								    <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Skills</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;">
									   <input type="text" class="form-control" id="skills" name="skills" placeholder="Skills">
									  </div>
								  </div>
								   
							   </form> 
							 </div>
						  </div>
						   <div class="col-md-6 right_min">
							  <div class="col-md-12 mil_e" style="padding: 0px;">
							   <form class="form-horizontal" action="" method="">
								  <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Company Name </label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;">
									    <select class="form-control input-sm" id="company_id" name="company_id">
	                                      <option value="None">Select Company</option>
	                                      <?php 
	                                        foreach ($companies as $row) {
	                                          echo '<option value='.$row['id'].'>'.$row['company_name'].'</option>';
	                                        }
	                                      ?>
	                                    </select>
									  </div>
								  </div> 
								   
								   <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Industry Name</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;">
									    <select class="form-control input-sm" id="industry_id" name="industry_id">
	                                      <option value="None">Select Industry</option>
	                                      <?php 
	                                        foreach ($industries as $row) {
	                                          echo '<option value='.$row['id'].'>'.$row['name'].'</option>';
	                                        }
	                                      ?>
	                                    </select>
									  </div>
								  </div> 
								   
								   <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Experience</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;">
									  	<div style="float: left;width: 13%;">From: </div>
									  	<div style="float: left;width: 35%;">
										    <select class="form-control input-sm" id="experience_from" name="experience_from">
		                                      <?php 
		                                        for ($i=0; $i<=30; $i++) {
		                                          echo '<option value='.$i.'>'.$i.'</option>';
		                                        }
		                                      ?>
		                                    </select>
	                                	</div>
	                                	<div style="float: left;width: 15%;text-align: center;">To: </div>
	                                	<div style="float: left;width: 37%;">
		                                    <select class="form-control input-sm" id="experience_to" name="experience_to">
		                                      <?php 
		                                        for ($i=0; $i<=30; $i++) {
		                                          echo '<option value='.$i.'>'.$i.'</option>';
		                                        }
		                                      ?>
		                                    </select>
		                                </div>
									  </div>
								  </div> 
								   
								   <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Education</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;">
									    <select class="form-control input-sm" data-live-search="true" id="education_id" name="education_id">
	                                      <option value="None">Select Education</option>
	                                      <?php 
	                                        foreach ($educations as $row) {
	                                          echo '<option value='.$row['id'].'>'.$row['qualification'].'</option>';
	                                        }
	                                      ?>
	                                    </select>
									  </div>
								  </div> 
								   
								    <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Currency</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;">
									 	<select class="form-control input-sm" id="salary_currency" name="salary_currency">
	                                      <option value="None">Select Currency</option>
	                                      <?php
	                                        foreach ($currencies as $row) {
	                                          echo '<option value='.$row['id'].'>'.$row['currency_name'].'</option>';
	                                        }
	                                      ?>
	                                    </select>
									  </div>
								</div>
								   
							    <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Salary</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;">
									  	<div style="float: left;width: 13%;">From:</div>
									  	<div style="float: left;width: 35%;">
										   	<select class="form-control input-sm" id="salary_from" name="salary_from">
		                                      <?php 
		                                        for ($i=0; $i<=5000000; $i=$i+100000) {
		                                          echo '<option value='.$i.'>'.$i.'</option>';
		                                        }
		                                      ?>
		                                    </select>
	                                	</div>
	                                	<div style="float: left;width: 15%; text-align: center;">To:</div>
	                                	<div style="float: left;width: 37%;">
										   	<select class="form-control input-sm" id="salary_to" name="salary_to">
		                                      <?php 
		                                        for ($i=0; $i<=5000000; $i=$i+100000) {
		                                          echo '<option value='.$i.'>'.$i.'</option>';
		                                        }
		                                      ?>
		                                    </select>
		                                </div>
									  </div>
							  	</div>

							  	<div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Total Positions</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;">
									  	<input class="form-control input-sm" id="positions" name="positions" placeholder="No. of positions" autocomplete="none"/>
									  </div>
							  	</div>

							  	<div class="form-group">
								    <div class="col-md-8" style="padding: 0px;">
									   <div style="cursor: pointer;" class="addPreQuestions" data-toggle="modal" data-target="#myModal1">Add Pre-screen Questions</div> &nbsp;&nbsp; (optional)
								  	</div>
							  	</div>
								   
							   </form> 
							 </div>
						  </div>

						  	<div class="form-group">
	                          	<div class="col-md-12" style="text-align: right;">
	                               	<!-- <button class="btn btn-primary btn-sm" name="tab1_draft" id="tab1_draft" type="button" ><span class="fa fa-save fw-fa"></span> Draft</button>
	                               	<button class="btn btn-primary btn-sm" name="tab1_next" id="tab1_next" type="button" ><span class="fa fa-save fw-fa"></span> Next</button> -->
	                            	<!-- <a href="index.php" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;<strong>Back</strong></a> -->

	                            	<a class="btn btn-info btnTab1Draft">Save Draft</a>
	                            	<a class="btn btn-primary btnNext" >Next</a>

	                          	</div>
                        	</div>

						</div>
					</div>
					<div id="tab2" class="container tab-pane fade">
						<br>
						<h3>Job Settings</h3>
						
						<div class="form-group">
                          <div class="col-md-12">
                            <label class="col-md-8" for="candidate_applications">How many candidate applications do you want to review for this position?</label>

                            <div class="col-md-8">
                              <input class="form-control input-sm" id="total_candidate_applications" name="total_candidate_applications" placeholder="Total candidate applications" autocomplete="none"/>
                              <div>(Min. 50 credits and Max. 200)</div>
                            </div>
                          </div>
                      	</div>

                      	<div class="form-group">
                          <div class="col-md-12">
                            <label class="col-md-6" for="candidates_interview">How many candidates do you want to interview for this position?</label>

                            <div class="col-md-8">
                              <input class="form-control input-sm" id="total_candidates_interview" name="total_candidates_interview" placeholder="Total candidates interview" autocomplete="none"/>
                            </div>
                          </div>
                      	</div>

                      	<div class="form-group">
                          <div class="col-md-12">
                            <label class="col-md-6" for="candidates_hire">How many candidates do you want to hire for this position?</label>

                            <div class="col-md-8">
                              <input class="form-control input-sm" id="total_candidates_hire" name="total_candidates_hire" placeholder="Total candidates hire" autocomplete="none"/>
                            </div>
                          </div>
                      	</div>


                      	<div class="col-md-12">You want to add reward amount or gifts for each stage</div>
                  	 
                          <div class="form-group">
                            <div class="col-md-12">
                              <label class="col-md-12" for="currency_applications">Set reward amount with currecy for each accepted applications</label>

                              <div class="col-md-2">
                                <select class="form-control input-sm" id="currency_applications" name="currency_applications">
                                  <option value="None">Select Currency</option>
                                  <?php
                                      foreach ($currencies as $row) {
                                        echo '<option value='.$row['id'].'>'.$row['currency_name'].'</option>';
                                      }
                                  ?>
                                </select>
                              </div>
                              <div class="col-md-1">
                                <input class="form-control input-sm" id="amount_applications" name="amount_applications" placeholder="Reward amount" autocomplete="none"/>
                              </div>

                              <div class="col-md-3">(Min. ₹1000 and Max. ₹5000)</div>

                              <div class="col-md-1"> OR </div>

                              <div class="col-md-2">
                                <select class="form-control input-sm" id="gift_applications" name="gift_applications">
                                  <option value="None">Select Gift</option>
                                  <?php
                                      foreach ($gifts as $row) {
                                        echo '<option value='.$row['id'].'>'.$row['name'].'</option>';
                                      }
                                  ?>
                                </select>
                              </div>

                              <div class="col-md-2">
                                <input class="form-control input-sm" id="gift_applications_amount" name="gift_applications_amount" placeholder="Gift description" autocomplete="none"/>
                              </div>
                            </div>
                          </div>

                        <div class="form-group">
                            <div class="col-md-12">
                              <label class="col-md-12" for="currency_interview">Set reward amount with currecy for scheduling to Interview</label>

                              <div class="col-md-2">
                                <select class="form-control input-sm" id="currency_interview" name="currency_interview">
                                  <option value="None">Select Currency</option>
                                  <?php
                                      foreach ($currencies as $row) {
                                        echo '<option value='.$row['id'].'>'.$row['currency_name'].'</option>';
                                      }
                                  ?>
                                </select>
                              </div>
                              <div class="col-md-1">
                                <input class="form-control input-sm" id="amount_interview" name="amount_interview" placeholder="Reward amount" autocomplete="none"/>
                              </div>

                              <div class="col-md-3">(Min. ₹1000 and Max. ₹5000)</div>

                              <div class="col-md-1"> OR </div>

                              <div class="col-md-2">
                                <select class="form-control input-sm" id="gift_interview" name="gift_interview">
                                  <option value="None">Select Gift</option>
                                  <?php
                                      foreach ($gifts as $row) {
                                        echo '<option value='.$row['id'].'>'.$row['name'].'</option>';
                                      }
                                  ?>
                                </select>
                              </div>

                              <div class="col-md-2">
                                <input class="form-control input-sm" id="gift_interview_amount" name="gift_interview_amount" placeholder="Gift description" autocomplete="none"/>
                              </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                              <label class="col-md-12" for="currency_offerletter">Set reward amount with currecy for offer letter release</label>

                              <div class="col-md-2">
                                <select class="form-control input-sm" id="currency_offerletter" name="currency_offerletter">
                                  <option value="None">Select Currency</option>
                                  <?php
                                      foreach ($currencies as $row) {
                                        echo '<option value='.$row['id'].'>'.$row['currency_name'].'</option>';
                                      }
                                  ?>
                                </select>
                              </div>
                              <div class="col-md-1">
                                <input class="form-control input-sm" id="amount_offerletter" name="amount_offerletter" placeholder="Reward amount" autocomplete="none"/>
                              </div>

                              <div class="col-md-3">(Min. ₹1500 and Max. ₹10000)</div>

                              <div class="col-md-1"> OR </div>

                              <div class="col-md-2">
                                <select class="form-control input-sm" id="gift_offerletter" name="gift_offerletter">
                                  <option value="None">Select Gift</option>
                                  <?php
                                      foreach ($gifts as $row) {
                                        echo '<option value='.$row['id'].'>'.$row['name'].'</option>';
                                      }
                                  ?>
                                </select>
                              </div>

                              <div class="col-md-2">
                                <input class="form-control input-sm" id="gift_offerletter_amount" name="gift_offerletter_amount" placeholder="Gift description" autocomplete="none"/>
                              </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                              <label class="col-md-12" for="currency_job">Set reward amount with currecy for joining for each Job</label>

                              <div class="col-md-2">
                                <select class="form-control input-sm" id="currency_job" name="currency_job">
                                  <option value="None">Select Currency</option>
                                  <?php
                                      foreach ($currencies as $row) {
                                        echo '<option value='.$row['id'].'>'.$row['currency_name'].'</option>';
                                      }
                                  ?>
                                </select>
                              </div>
                              <div class="col-md-1">
                                <input class="form-control input-sm" id="amount_job" name="amount_job" placeholder="Reward amount" autocomplete="none"/>
                              </div>

                              <div class="col-md-3">(Min. ₹4000 and Max. ₹20000)</div>

                              <div class="col-md-1"> OR </div>

                              <div class="col-md-2">
                                <select class="form-control input-sm" id="gift_job" name="gift_job">
                                  <option value="None">Select Gift</option>
                                  <?php
                                      foreach ($gifts as $row) {
                                        echo '<option value='.$row['id'].'>'.$row['name'].'</option>';
                                      }
                                  ?>
                                </select>
                              </div>

                              <div class="col-md-2">
                                <input class="form-control input-sm" id="gift_job_amount" name="gift_job_amount" placeholder="Gift description" autocomplete="none"/>
                              </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12" style="text-align: right;">
								<a class="btn btn-primary btnPrevious">Previous</a>
								<a class="btn btn-info btnTab2Draft">Save Draft</a>
		        				<a class="btn btn-primary btnNext" onclick="showJobPreview();">Next</a>
		        			</div>
		        		</div>
					</div>
					<div id="tab3" class="container tab-pane fade">
						<br>
						<h3>Job Preview</h3>

						


						<div class="col-md-12 col-sm-12 col-xs-12 creat_j">
						  <div class="col-md-6 left_min">
							 <div class="col-md-12 mil_e" style="padding: 0px;">
							   <form class="form-horizontal" action="" method="">
								  <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Job ID </label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;" id="preview_jobid">
									  </div>
								  </div> 
								   
								   <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Job Title</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;" id="preview_jobtitle">
									  </div>
								  </div> 
								   
								   <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Location</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;" id="preview_location">
									  </div>
								  </div> 
								   
								   <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Job Type</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;" id="preview_job_type">
									  </div>
								  </div> 
								   
								    <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Summary</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;" id="preview_summary">
									  </div>
								  </div>
								   
								    <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Skills</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;" id="preview_skills">
									  </div>
								  </div>
								   
							   </form> 
							 </div>
						  </div>
						   <div class="col-md-6 right_min">
							  <div class="col-md-12 mil_e" style="padding: 0px;">
							   <form class="form-horizontal" action="" method="">
								  <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Company Name </label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;" id="preview_company">
									  </div>
								  </div> 
								   
								   <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Industry Name</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;" id="preview_industry">
									  </div>
								  </div> 
								   
								   <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Experience</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;" id="preview_experience">
									  </div>
								  </div> 
								   
								   <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Education</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;" id="preview_education">
									  </div>
								  </div> 
								   
								    <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Currency</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;" id="preview_salary_currency">
									  </div>
								</div>
								   
							    <div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Salary</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;" id="preview_salary">
									  </div>
							  	</div>

							  	<div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <label class="leb_p">Total Positions</label>  
									</div> 
									  <div class="col-md-9" style="padding: 0px;" id="preview_positions">
									  </div>
							  	</div>

							  	<div class="form-group">
								    <div class="col-md-3" style="padding: 0px;">
									   <div style="cursor: pointer;" onClick="javascript: addPreQuestions();" class="addPreQuestions">Add Pre-screen Questions</div> &nbsp;&nbsp; (optional)
								  	</div>
							  	</div>
								   
							   </form> 
							 </div>
						  </div>


						<h3>Job Settings</h3>

						<div class="form-group">
                          <div class="col-md-12">
                            <label class="col-md-8">How many candidate applications do you want to review for this position?</label>

                            <div class="col-md-8" id="preview_total_candidate_applications">
                            </div>
                          </div>
                      	</div>

                      	<div class="form-group">
                          <div class="col-md-12">
                            <label class="col-md-6">How many candidates do you want to interview for this position?</label>

                            <div class="col-md-8" id="preview_total_candidates_interview">
                            </div>
                          </div>
                      	</div>

                      	<div class="form-group">
                          <div class="col-md-12">
                            <label class="col-md-6">How many candidates do you want to hire for this position?</label>

                            <div class="col-md-8" id="preview_total_candidates_hire">
                            </div>
                          </div>
                      	</div>


                      	<div class="col-md-12">You want to add reward amount or gifts for each stage</div>
                  	 
                          <div class="form-group">
                            <div class="col-md-12">
                              <label class="col-md-12">Set reward amount with currecy for each accepted applications</label>

                              <div class="col-md-2" id="preview_currency_applications">
                              </div>
                            </div>
                          </div>

                        <div class="form-group">
                            <div class="col-md-12">
                              <label class="col-md-12">Set reward amount with currecy for scheduling to Interview</label>

                              <div class="col-md-2" id="preview_currency_interview">
                              </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                              <label class="col-md-12">Set reward amount with currecy for offer letter release</label>

                              <div class="col-md-2" id="preview_currency_offerletter">
                              </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                              <label class="col-md-12">Set reward amount with currecy for joining for each Job</label>

                              <div class="col-md-2" id="preview_currency_job">
                              </div>
                            </div>
                        </div>

					</div>




						<div class="form-group">
                            <div class="col-md-12" style="text-align: right;">
								<a class="btn btn-primary btnPrevious">Previous</a>
								<a class="btn btn-info btnTab3Draft">Save Draft</a>
								<a class="btn btn-primary">Submit</a>
							</div>
						</div>
					</div>
				</div>
                </div>
				
				
				
            </div><!---------- col-md-9 main ------------->
			
			

            <!-- <nav aria-label="Page navigation example" class="pull-right">
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                </ul>
            </nav> -->

        </div>
        <!------------ full body -------------->

    </div>

    <script>
    	/* Job Preview content */
		function showJobPreview() {
			$('#preview_jobid').html($('#jobid').val());
			$('#preview_jobtitle').html($('#job_title').val());
			$('#preview_location').html($("#location option[value="+$('#location').val()+"]").text());
			$('#preview_job_type').html($("#job_type option[value="+$('#job_type').val()+"]").text());
			$('#preview_summary').html($('#summary').val());
			$('#preview_skills').html($('#skills').val());
			$('#preview_company').html($("#company_id option[value="+$('#company_id').val()+"]").text());
			$('#preview_industry').html($("#industry_id option[value="+$('#industry_id').val()+"]").text());
			$('#preview_experience').html($('#experience_from').val()+" - " +$('#experience_to').val()+" years");
			$('#preview_education').html($("#education_id option[value="+$('#education_id').val()+"]").text());
			$('#preview_salary_currency').html($("#salary_currency option[value="+$('#salary_currency').val()+"]").text());
			$('#preview_salary').html($('#salary_from').val()+" - " +$('#salary_to').val());
			$('#preview_positions').html($('#positions').val());
			

			$('#preview_total_candidate_applications').html($('#total_candidate_applications').val());
			$('#preview_total_candidates_interview').html($('#total_candidates_interview').val());
			$('#preview_total_candidates_hire').html($('#total_candidates_hire').val());


			if($('#currency_applications').val() != '')
				var amtApplication = $("#currency_applications option[value="+$('#currency_applications').val()+"]").text() + " " + $("#amount_applications option[value="+$('#amount_applications').val()+"]").text();
			else
				var amtApplication = $("#gift_applications option[value="+$('#gift_applications').val()+"]").text() + " " + $("#gift_applications_amount option[value="+$('#gift_applications_amount').val()+"]").text()
			$('#preview_currency_applications').html(amtApplication);

			if($('#currency_interview').val() != '')
				var amtInterview = $("#currency_interview option[value="+$('#currency_interview').val()+"]").text() + " " + $("#amount_interview option[value="+$('#amount_interview').val()+"]").text();
			else
				var amtInterview = $("#gift_interview option[value="+$('#gift_interview').val()+"]").text() + " " + $("#gift_interview_amount option[value="+$('#gift_interview_amount').val()+"]").text()
			$('#preview_currency_interview').html(amtInterview);

			if($('#currency_offerletter').val() != '')
				var amtOfferletter = $("#currency_offerletter option[value="+$('#currency_offerletter').val()+"]").text() + " " + $("#amount_offerletter option[value="+$('#amount_offerletter').val()+"]").text();
			else
				var amtOfferletter = $("#gift_offerletter option[value="+$('#gift_offerletter').val()+"]").text() + " " + $("#gift_offerletter_amount option[value="+$('#gift_offerletter_amount').val()+"]").text()
			$('#preview_currency_offerletter').html(amtOfferletter);


			if($('#currency_job').val() != '')
				var amtJob = $("#currency_job option[value="+$('#currency_job').val()+"]").text() + " " + $("#amount_job option[value="+$('#amount_job').val()+"]").text();
			else
				var amtJob = $("#gift_job option[value="+$('#gift_job').val()+"]").text() + " " + $("#gift_job_amount option[value="+$('#gift_job_amount').val()+"]").text()
			$('#preview_currency_job').html(amtJob);

			 
			//$('#preview_industry').html($("#industry_id option[value="+$('#industry_id').val()+"]").text());
			

			//$("#myselect option[value=2]").text();
		}
	</script>

    <script>

	  	$('.btnNext').click(function(){
		  $('.nav-tabs > .active').next('li').find('a').trigger('click');
		});

	  	$('.btnPrevious').click(function(){
		  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
		});

	</script>


  <!-- Modal -->
  <div class="modal fade model_b" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header header_p">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Pre-Screen Questions</h4>
        </div>
        <div class="modal-body" style="min-height: 250px; max-height: 250px; overflow-y: scroll;">
           	<form class="form-horizontal" action="" method="" id="">
			  	<div class="col-md-10 form-group pre_questions">
				  	<div class="col-md-10 leb_l form-group order_number">
		        		<input type="text" class="form-control" id="question[]" name="question[]" placeholder="Add question"> <span onclick="removeDiv(this)"><i class="fa fa-minus-circle" aria-hidden="true"></i></span>
				  	</div>
			  	</div>
		   	</form>
        </div>
        <div class="modal-footer mo_fo" style="text-align: right;">
        	<div class="form-group col-md-6" style="text-align: left;">
				<a class="btn btn-info btnTab3Draft" data-dismiss="modal">Cancel</a>
				<a class="btn btn-primary" onclick="saveQuestions();">Save</a>
			</div>
          	<div class="form-group col-md-6" style="text-align: right;" onclick="javascript: appendToDiv();"><i class="fa fa-plus" aria-hidden="true"></i></div>
          </div>
        </div>
      </div>
      
    </div>
  </div>

  <script>
  	function appendToDiv() {
  		$(".pre_questions").append('<div class="col-md-10 leb_l form-group order_number"><input type="text" class="form-control" id="question[]" name="question[]" placeholder="Add question"> <span onclick="removeDiv(this)"><i class="fa fa-minus-circle" aria-hidden="true"></i></span></div>');
 	}

 	/*$('.fa-minus-circle').on('click',function(){
 		alert('Here');
	    $(this).parent('div.order_number').remove();
	});*/

	function removeDiv(divID) {
		//$(this).parent('div.order_number').remove();
		$(divID).parent('div').remove();
	}

	function saveQuestions() {

		var question = $('input[name="question[]"]').map(function(){ 
            return this.value; 
        }).get();

		$.ajax({
			url: "../../user/jobs/savePreScreenQuestions",
			type: "post",
			data: { 'question[]': question, },
           	success: function(result) {
        	    //$("#h11").html(result);
        	    alert(result);
        	}
        });

	}
  </script>

<!-- Sticky Footer -->
<?php include APPPATH.'views/user/includes/footer.php';?>