<?php include APPPATH.'views/user/includes/header.php';?>

<div class="col-md-12 col-sm-12 col-xs-12 top_search">
    <div class="container">
        <h4 class="text-center h4_p">My Job Page</h4>
    </div>
</div>

<div class="">
    <div class="col-md-12 col-sm-12 col-xs-12 full_box" style="margin-top: 25px;">
        
        <?php include_once('includes/sidebar.php'); ?>

        <div class="col-md-9 col-sm-6 col-xs-12 right_job">
            <div class="col-md-12 body_1" style="padding: 0px;">
                <div class="col_md-12 job_d">
                    <h4 class="Search_Jobs">Search By</h4>
                    <div class="fomr_b">
                        <form class="form-horizontal" action="" method="" id="">
                            <div class="col-md-12 tw_o">
                                <div class="col-md-4 le_l" style="padding: 0px;">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" name="" id="" placeholder="Job Title or Keyword" </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3" style="padding: 0px;">
                                    <div class="col-md-12 le_r">
                                        <div class="form-group">
                                            <div class="col-md-12 mob_pa">
                                              <select class="selectpicker form-control">
                      												  <option>Location</option>
                      												  <option>Location 1</option>
                      												  <option>Location 2</option>
                      												  <option>Location 3</option>
                      												  <option>Location 4</option>
                      												  <option>Location 5</option>
                      											  </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>
								
								                <div class="col-md-3" style="padding: 0px;">
                                    <div class="col-md-12 le_r">
                                        <div class="form-group">
                                            <div class="col-md-12 mob_pa">
                                              <select class="selectpicker form-control">
                      												  <option>Created Form Date</option>
                      												  <option>Created Form Date 1</option>
                      												  <option>Created Form Date 2</option>
                      												  <option>Created Form Date 3</option>
                      												  <option>Created Form Date 4</option>
                      												  <option>Created Form Date 5</option>
                      											  </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>
								
							                  <div class="col-md-2" style="padding: 0px;">
                                    <div class="col-md-12 le_r">
                                        <div class="form-group">
                                            <div class="col-md-12 mob_pa">
                                              <select class="selectpicker form-control">
                      												  <option>Created To Date</option>
                      												  <option>Created To Date 1</option>
                      												  <option>Created To Date 2</option>
                      												  <option>Created To Date 3</option>
                      												  <option>Created To Date 4</option>
                      												  <option>Created To Date 5</option>
                      											  </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                        </form>

                        </div>
                    </div>

                </div>
            </div>
			
			<div class="col-md-12 col-sm-12 col-xs-12 data_tab">
			       <div class="table-responsive">
        <table class="table table-bordered table-hover">
          <thead class="head_co">
            <tr>
              <th class="text-center">Job Id</th>
              <th class="text-center">Job Title</th>
              <th class="text-center">Location</th>
              <th class="text-center">Created/Upload</th>
              <th class="text-center">Total Position</th>
			        <th class="text-center">Paid Amount</th>
              <th class="text-center">Status</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody class="bod_t">
          <?php if(count($result) > 0) { 
                  foreach ($result as $value) {
            ?>
              <tr>
                <td><?php echo $value['id']; ?></td>
                <td><?php echo $value['title']; ?></td>
                <td><?php echo $value['location']; ?></td>
                <td><?php echo $value['created_at']; ?></td>
                <td><?php echo $value['total_positions']; ?></td>
  			        <td><?php echo $value['currency'].' '.$value['total_rewards']; ?></td>
                <td><?php echo $value['status']; ?></td>
                <td><a href="#" class="" data-toggle="modal" data-target="#mypop">View / Schedule</a></td>
              </tr>
          <?php }
          } else { ?>
              <tr>
                <td colspan="8" align="center">No Data Found</td>
              </tr>
          <?php } ?>
            
          </tbody>
        </table>
      </div><!--end of .table-responsive-->
			</div>

            <nav aria-label="Page navigation example" class="pull-right">
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                </ul>
            </nav>

        </div>
        <!------------ full body -------------->

    </div>

    <div class="col-md-12 col-sm-12 col-xs-12 footer">
        <div class="col-md-6 left_f">
            <ul class="list-inline">
                <li><a href="#">About us </a></li>
                <li><a href="#">Contact us</a></li>
                <li><a href="#">Teams and Conditions</a></li>
            </ul>
        </div>
        <div class="col-md-6 right_f">
            <span class="co_r">© 2020 ReferXtra Limited. All rights reserved.</span>
        </div>
    </div>
	
	<!-- Modal -->
  <div class="modal fade model_b" id="mypop" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header header_p">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Schedule Interview</h4>
        </div>
        <div class="modal-body">
          <div class="col-md-12 mod_bo">
			  <div class="col-md-6 left_mb" style="padding: 0px;">
			     <table class="table tab_cs">
				  <tbody class="chan_cs">
				  <tr>
					 <td><strong>Job ID :</strong></td>
					 <td>1001</td>
					</tr>
						 <tr>
					 <td><strong>Job Title :</strong></td>
					 <td>PHP Developer</td>
					</tr>
							<tr>
					 <td><strong>Location :</strong></td>
					  <td>Hyderabad, India</td>
					</tr>
							<tr>
					  <td><strong>Job Type :</strong></td>
					  <td>Full Time</td>
					</tr>
							<tr>
					  <td><strong>Summary :</strong></td>
					  <td>A PHP Developer is responsible for creating
						and implementing an array of Web-based products
						using PHP,MySQL,Ajax,and JavaScript.You
						Develop back-end components</td>
					</tr>
						  <tr>
					  <td><strong>Skills :</strong></td>
					  <td>PHP,MySQL,HTML,CSS</td>
					</tr>

				  </tbody>
				</table>
			  </div>
			  
			  <div class="col-md-6 left_mb" style="padding: 0px;">
			      	     <table class="table tab_cs">
				  <tbody class="chan_cs">
				  <tr>
					 <td><strong>Company Name :</strong></td>
					 <td>Infosys</td>
					</tr>
						 <tr>
					 <td><strong>Experience Form :</strong></td>
					 <td>5</td>
					</tr>
							<tr>
					 <td><strong>Experience To :</strong></td>
					  <td>10</td>
					</tr>
							<tr>
					  <td><strong>Education :</strong></td>
					  <td>Post Graduate</td>
					</tr>
							<tr>
					  <td><strong>Min Salary :</strong></td>
					  <td>INR 500000</td>
					</tr>
						  <tr>
					  <td><strong>Max Salary :</strong></td>
					  <td>INR 1000000</td>
					</tr>
					   <tr>
					  <td><strong>Total Positions :</strong></td>
					  <td>5</td>
					</tr>

				  </tbody>
				</table>
			  </div>
			  
		  </div>
			
			 <div class="col-md-12 leg_d">
				    <div class="col-md-12 u_l">
						<div class="col-md-6 left_ph" style="padding: 0px;">
						   <div class="col-md-12 label_p">
							<h3 class="inter_v">Interview Dtae and Time</h3>
							</div>
							<div class="col-md-12 label_m">
							   <div class="styled-select slect_r">
								   <select>
									 <option>Schedule Dtae & Time</option>
									 <option>Schedule Dtae & Time</option>
									 <option>Schedule Dtae & Time</option>
								   </select>
								  <span class="fa fa-sort-desc"></span>
								</div>
							</div>
						</div>
						
						<div class="col-md-6 right_ph" style="padding: 0px;">
						   <div class="col-md-12 label_p">
							<h3 class="inter_v">Interview Type</h3>
							</div>
							<div class="col-md-12 label_m">
							   <div class="styled-select slect_r">
								   <select>
									 <option>Telephone</option>
									 <option>Telephone</option>
									 <option>Telephone</option>
								   </select>
								  <span class="fa fa-sort-desc"></span>
								</div>
							</div>
						</div>
					</div> 

			 </div>
			
        </div>
        <div class="modal-footer bordr_t">
          <button type="button" class="btn btn-default rde" data-dismiss="modal">Cancel</button>
			 <button type="button" class="btn btn-default prim">Schedule</button>
        </div>
      </div>
      
    </div>
  </div>
  
<script src="js/local.js"></script>
</body>
</html>
