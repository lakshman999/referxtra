 
 <?php include_once('includes/header.php'); ?>

  <div class="col-md-12 col-sm-12 col-xs-12 top_search">
    <div class="container">
      <h4 class="text-center h4_p">Search Jobs</h4>
    </div>
  </div>
  
  <div class="">
     <div class="col-md-12 col-sm-12 col-xs-12 full_box" style="margin-top: 25px;">
      
      <?php include_once('includes/sidebar.php'); ?>
       
      <div class="col-md-9 col-sm-6 col-xs-12 right_job">
           <div class="col-md-12 body_1" style="padding: 0px;">
          <div class="col_md-12 job_d">
           <h4 class="Search_Jobs">Search Jobs</h4>
          <div class="fomr_b">
             <form class="form-horizontal" action="" method="" id="">
               <div class="col-md-12 tw_o">
                 <div class="col-md-6 le_l">
                <div class="form-group">
                  <div class="col-md-12">
                  <label>What is the Job?</label>
                </div>
                <div class="col-md-12">
                  <input type="text" class="form-control" name="" id="" placeholder="Job title or keyboard"/>
                  
                 </div>
               </div>
                </div>
                 
                 <div class="col-md-6">
                 <div class="col-md-12 le_r">
                <div class="form-group">
                  <div class="col-md-12">
                   <label>where is the job?</label> 
                </div>
                  <div class="col-md-12">
                  <input type="text" class="form-control" name="" id="" placeholder="Job title or keyboard" />
                    
                 </div>
               </div>
                 
               </div>
                   </div>
            
             </form>
          
        </div>
       </div>
        
            </div></div><br>
           
         <div class="clearfix"></div>
        
        <?php
        //echo '<pre>'; print_r($searchJobs); 
        if(count($searchJobs) > 0) {
          foreach ($searchJobs as $result) {
        ?>
         <div class="col-md-12 ne_t" style="padding: 0px;">
             <p class="pull-right p_r"><?php echo $result['job_type']; ?></p>
           
           <div class="col-md-12 fu_p">
              <div class="col-md-4 one_e1">
                 <h3 class="comp_r"><span><?php echo $result['title']; ?></span> Exp: <?php echo $result['experience_from'].' - '.$result['experience_to'].' Yrs'; ?></h3>
                <p><?php echo $result['company_name']; ?></p>
                <h3 class="comp_r">Job Summary:</h3>
                <p><?php 
                    if(strlen($result['summary']) > 500)
                      echo substr($result['summary'], 0, 500).' ...';
                    else {
                       echo $result['summary'];
                     }
                  ?></p>
                <span><a href="#" class="te_t" style="color: #00f;">See Full Description</a></span>&nbsp;&nbsp;&nbsp;<span><a  class="te_t" href="#">Posted On <?php echo date('Y-m-d', strtotime($result['created_at'])); ?></a></span>
              
              <!-- <div class="col-md-2 one_e"> -->
                <!-- <img src="img/IBM_logo.svg" width="100" alt="com_loo" title="com_loo" class="img-responsive ib_m"> -->
                <div class="loc_c"><i class="fa fa-map-marker fa_l"></i> <?php echo $result['location']; ?></div>
              <!-- </div> -->
              </div>

              <div class="col-md-3 one_e2">
                 <h3 class="comp_r">Referral Reward</h3>
                <p>Application</p>
                <h3 class="comp_r">Application</h3>
                <p><i class="fa fa-rupee fa_r"></i><?php echo $result['accepted_application_rewards']; ?></p>
                <h3 class="comp_r">Interview</h3>
                <p><i class="fa fa-rupee fa_r"></i><?php echo $result['schedule_interview_rewards']; ?></p>
                 <h3 class="comp_r">Hire</h3>
                <p><i class="fa fa-rupee fa_r"></i><?php echo $result['offerletter_release_rewards']; ?></p>
                 <h3 class="comp_r">Total Potential Rewards</h3>
                <p><i class="fa fa-rupee fa_r"></i><?php echo $result['total_rewards']; ?></p>
                <button type="button" class="btn btn-danger btn_p">Apply</button>
              </div>
              <div class="col-md-3 one_e3">
              <p class="lin_h">Get link to refer this job.</p>
              <p class="lin_h">Refer this job to your networks</p>
              
              <button type="button" class="btn btn-primary btn_ea" data-toggle="modal" data-target="#myModal1">Refer and Earn</button>
              </div>
             
           </div>
           </div>
        <?php } ?>
        
          
        <nav aria-label="Page navigation example" class="pull-right">
          <ul class="pagination">
          <li class="page-item"><a class="page-link" href="#">Previous</a></li>
          <li class="page-item"><a class="page-link" href="#">1</a></li>
          <li class="page-item"><a class="page-link" href="#">2</a></li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item"><a class="page-link" href="#">Next</a></li>
          </ul>
        </nav>

      <?php } else { ?>
            <div class="col-md-12 ne_t" style="padding: 0px;">No data found</div>
        <?php } ?>
     </div>
  <!------------ full body -------------->
      
          
    </div>
  
  
  <?php include_once('includes/footer.php'); ?>
  
   <!-- Modal -->
  <div class="modal fade model_b" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header header_p">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Refer to Friend(s)</h4>
        </div>
        <div class="modal-body">
           <form class="form-horizontal" action="" method="" id="">
        <div class="form-group">
          <div class="col-md-2 leb_l">
               <label>To Email(s)</label> 
          </div>
          <div class="col-md-10 lef_r">
            <input type="text" class="form-control" action="" method="" id="" placeholder="Email(s)">
          </div>
        </div>
           <div class="form-group">
          <div class="col-md-2 leb_l">
               <label>Subject </label> 
          </div>
          <div class="col-md-10 lef_r">
            <input type="text" class="form-control" action="" method="" id="" placeholder="KARDS Cyber Solutions is hiring and I thought of you!">
          </div>
        </div>
              <div class="form-group">
          <div class="col-md-2 leb_l">
               <label>Password </label> 
          </div>
          <div class="col-md-10 lef_r">
            <input type="Password" class="form-control" action="" method="" id="" placeholder="Password">
          </div>
        </div>
       </form>
        </div>
        <div class="modal-footer mo_fo">
          <button type="button" class="btn btn-default rde" data-dismiss="modal">Cancel</button>
       <button type="button" class="btn btn-default prim">Send</button>
        </div>
      </div>
      
    </div>
  </div>
  
<script src="../../assests/js/local.js"></script>
</body>
</html>
