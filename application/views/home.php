<?php include_once 'user/includes/header.php';?>

<link rel="stylesheet" href="../assests/css/style.css" type="text/css">

<style type="text/css">
  body {
    background: gray url("../assests/images/slider-bg12.jpg") repeat 0 0;
    font-family: 'Titillium Web', sans-serif !important;
  }
  .footer {
      background: #fff !important;
      padding: 10px;
      padding-top: 18px;
      position: absolute;
      bottom: 0px;
  }
</style>
    <div class="container">
      <div class="col-md-12 col-sm-12 col-xs-12 tabs_p">
        <div class="tabs_b">
          <div class="tab-button-outer">
            <ul id="tab-button">
              <li><a href="#tab01" class="fir_b">Post Job</a></li>
              <li><a href="#tab02" class="fir_b">Refer Job</a></li>
              <li><a href="#tab03" class="fir_b">Search Job</a></li>
            </ul>
          </div>
          <div class="tab-select-outer">
            <select id="tab-select">
              <option value="#tab01">Post Job</option>
              <option value="#tab02">Refer Job</option>
              <option value="#tab03">Search Job</option>
            </select>
          </div>

          <div id="tab01" class="tab-contents">
            <h2 class="cl_s hidden-lg hidden-md hidden-sm">Post Job</h2>
              <form class="form-horizontal" action="" method="" >
               <div class="col-md-4" style="padding: 0px;">
                 <div class="form-group has-search">
                  <span class="fa fa-search form-control-feedback"></span>
                 <input type="text" class="form-control fo_c" id="" name="" placeholder="Job Title" />
               </div>
             </div>
               <div class="col-md-3" style="padding: 0px;">
                 <div class="form-group has-search">
                  <span class="fa fa-search form-control-feedback"></span>
                 <input type="text" class="form-control fo_c" id="" name="" placeholder="Skills" />
               </div>
             </div>
              <div class="col-md-3" style="padding: 0px;">
                 <div class="form-group has-search">
                  <span class="fa fa-map-marker form-control-feedback"></span>
                 <input type="text" class="form-control fo_c" id="" name="" placeholder="Location" />
               </div>
             </div>
               <div class="col-md-2" style="padding: 0px;">
                 <div class="form-group">
                 <input type="submit" class="btn btn-danger fo_c no-radius" name="button" value="Search">
               </div>
             </div>
            </form>
          </div>


          <div id="tab02" class="tab-contents">
            <h2 class="cl_s hidden-lg hidden-md hidden-sm">Refer Job</h2>
             <form class="form-horizontal" action="" method="" >
               <div class="col-md-4" style="padding: 0px;">
                 <div class="form-group has-search">
                  <span class="fa fa-search form-control-feedback"></span>
                 <input type="text" class="form-control fo_c" id="" name="" placeholder="Job Title" />
               </div>
             </div>
              <div class="col-md-3" style="padding: 0px;">
                 <div class="form-group has-search">
                  <span class="fa fa-search form-control-feedback"></span>
                 <input type="text" class="form-control fo_c" id="" name="" placeholder="Company Name" />
               </div>
             </div>
              <div class="col-md-3" style="padding: 0px;">
                 <div class="form-group has-search">
                  <span class="fa fa-map-marker form-control-feedback"></span>
                 <input type="text" class="form-control fo_c" id="" name="" placeholder="Location" />
               </div>
             </div>
               <div class="col-md-2" style="padding: 0px;">
                 <div class="form-group">
                 <input type="submit" class="btn btn-danger fo_c no-radius" name="button" value="Refer Job">
               </div>
             </div>
            </form>
          </div>


          <div id="tab03" class="tab-contents">
            <h2 class="cl_s hidden-lg hidden-md hidden-sm">Search Job</h2>
             <form class="form-horizontal" action="./user/jobs/filterJobs" method="POST" >
               <div class="col-md-6" style="padding: 0px;">
                 <div class="form-group has-search">
                  <span class="fa fa-search form-control-feedback"></span>
                 <input type="text" class="form-control fo_c" id="search_job_title" name="search_job_title" placeholder="Job Title or Keywords" />
               </div>
             </div>
              <div class="col-md-4" style="padding: 0px;">
                 <div class="form-group has-search">
                  <span class="fa fa-map-marker form-control-feedback"></span>
                 <input type="text" class="form-control fo_c" id="search_location" name="search_location" placeholder="Location" />
               </div>
             </div>
               <div class="col-md-2" style="padding: 0px;">
                 <div class="form-group">
                 <input type="submit" class="btn btn-danger fo_c no-radius" name="search_job" id="search_job" value="Search Job">
               </div>
             </div>
            </form>
          </div>

        </div>
    </div>
  </div>

<script src="../assests/js/local.js"></script>

<?php include_once 'user/includes/footer.php';?>