<!DOCTYPE html>
<html lang="en">

<head>
<title>User Profile</title>
<!-- Bootstrap core CSS-->
<?php echo link_tag('assests/vendor/bootstrap/css/bootstrap.min.css'); ?>
<!-- Custom fonts for this template-->
<?php echo link_tag('assests/vendor/fontawesome-free/css/all.min.css'); ?>
<!-- Page level plugin CSS-->
<?php echo link_tag('assests/vendor/datatables/dataTables.bootstrap4.css'); ?>
<!-- Custom styles for this template-->
<?php echo link_tag('assests/css/sb-admin.css'); ?>


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

<script>

    function addPreQuestions() {
      //$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});

      $.ajax({    //create an ajax request to load_page.php
        //type:"POST",  
        url: "../vacancy/controller.php?action=display_prescreen_questions",    
        dataType: "JSON",  //expect html to be returned  
        //data:{USERNAME:username.value,PASS:pass.value},               
        success: function(data){
          //alert('Here');
          alert(data);
          
          //$('#loginerrormessage').html(data);
        }
      });

      //var post_data = {'<?php //echo $this->security->get_csrf_token_name(); ?>' : '<?php //echo $this->security->get_csrf_hash(); ?>'};

      /*$.ajax({
          url : "../vacancy/index.php?view=addQuestion",
          //url: '<?php //echo web_root."vacancy/index.php?view=addQuestion";?>',
          //method:'GET',
          //dataType:'json',
          type:'GET',
          //dataType: 'html',
          //data: post_data,
          success:function(response) {
            //$('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});
            
            console.log(response);
            $('#show_modal').append(response);
            
            //alert(response); return false;
            $('#show_modal').modal({backdrop: 'static', keyboard: true, show: true});

        }
      });*/
    }

    function closeModel() {
      $('#show_modal').hide();
      $('#show_modal').removeClass('in');
    }

  $(document).ready(function() {
      $('.nav-tabs > li > a').click(function(event){
          event.preventDefault();//stop browser to take action for clicked anchor
          
          //get displaying tab content jQuery selector
          var active_tab_selector = $('.nav-tabs > li.active > a').attr('href');          
          
          //find actived navigation and remove 'active' css
          var actived_nav = $('.nav-tabs > li.active');
          actived_nav.removeClass('active');
          
          //add 'active' css into clicked navigation
          $(this).parents('li').addClass('in');
          $(this).parents('li').addClass('active');
          
          //hide displaying tab content
          $(active_tab_selector).removeClass('active');
          $(active_tab_selector).removeClass('in');
          $(active_tab_selector).addClass('hide');

          //show target tab content
          var target_tab_selector = $(this).attr('href');
          $(target_tab_selector).removeClass('hide');
          $(target_tab_selector).addClass('in');
          $(target_tab_selector).addClass('active');
      });
      
  });

</script>

<style type="text/css">
  .active{
    display: block;
  }
  
  .hide{
    display: none;
  }
</style>

  </head>

  <body id="page-top">

   <?php include APPPATH.'views/admin/includes/header.php';?>

    <div id="wrapper">

      <!-- Sidebar -->
  <?php include APPPATH.'views/admin/includes/sidebar.php';?>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo site_url('admin/Manage_Jobs'); ?>">Jobs</a>
            </li>
            <li class="breadcrumb-item active">Create Job</li>
          </ol>

          <!-- Page Content -->
          <h3>Create Job</h3>
          <hr>
          <!---- Success Message ---->
            <form class="form-horizontal span6" action="./createJob" method="POST">
              <?php $tab = (isset($_GET['tab'])) ? $_GET['tab'] : ''; ?>

              <ul class="nav nav-tabs" id="myTabs"> 
                  <li id="li_tab1" class="active"><a href="#tabs1-pane1" id="tab1" data-toggle="tab">job Details</a></li> 
                  <li id="li_tab2"><a href="#tabs2-pane1" id="tab2" data-toggle="tab">job Settings</a></li> 
                  <li id="li_tab3"><a href="#tabs3-pane1" id="tab3" data-toggle="tab">Job Preview</a></li> 
              </ul>

                <div class="tab-pane fade in active <?php echo ($tab == 'jobDetails') ? 'active' : ''; ?>" id="tabs1-pane1">

                          <div class="col-sm-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                  <label class="col-md-3 control-label" for="Job ID">Job ID</label>

                                  <div class="col-md-8">
                                    <input class="form-control input-sm" id="jobid" name="jobid" placeholder="Job ID" autocomplete="none"/>
                                  </div>
                                </div>
                            </div>

                            <div class="form-group">
                              <div class="col-md-12">
                                <label class="col-md-3 control-label" for="job_title">Job Title</label> 
                                <div class="col-md-8">
                                  <input class="form-control input-sm" id="job_title" name="job_title" placeholder="Job Title" autocomplete="none"/> 
                                </div>
                              </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                  <label class="col-md-3 control-label" for="location">Location</label>

                                  <div class="col-md-8">
                                    <select class="form-control input-sm" id="location" name="location">
                                      <option value="None">Select</option>
                                      <?php 
                                        foreach ($locations as $row) {
                                          # code...
                                          echo '<option value='.$row['id'].'>'.$row['location'].'</option>';
                                        }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="col-md-12">
                                  <label class="col-md-3 control-label" for="job_type">Job Type</label>

                                  <div class="col-md-8">
                                    <select class="form-control input-sm" id="job_type" name="job_type">
                                      <option value="None">Select</option>
                                      <?php 
                                        foreach ($jobtypes as $row) {
                                          # code...
                                          echo '<option value='.$row['id'].'>'.$row['name'].'</option>';
                                        }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="col-md-12">
                                  <label class="col-md-3 control-label" for="summary">Summary</label>

                                  <div class="col-md-8">
                                    <textarea class="form-control input-sm" id="summary" name="summary" placeholder="Summary" autocomplete="none"></textarea>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="col-md-12">
                                  <label class="col-md-3 control-label" for="skills">Skills</label>

                                  <div class="col-md-8">
                                    <input class="form-control input-sm" id="skills" name="skills" placeholder="Skills" autocomplete="none"/>
                                  </div>
                                </div>
                              </div>
                          </div>

                          <div class="col-sm-6">
                              <div class="form-group">
                                <div class="col-md-12">
                                  <label class="col-md-5 control-label" for="company_name">Company Name</label>

                                  <div class="col-md-7">
                                    <select class="form-control input-sm" id="company_id" name="company_id">
                                      <option value="None">Select</option>
                                      <?php 
                                        foreach ($companies as $row) {
                                          echo '<option value='.$row['id'].'>'.$row['company_name'].'</option>';
                                        }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="col-md-12">
                                  <label class="col-md-5 control-label" for="industry_name">Industry Name</label>

                                  <div class="col-md-7">
                                    <select class="form-control input-sm" id="industry_id" name="industry_id">
                                      <option value="None">Select</option>
                                      <?php 
                                        foreach ($industries as $row) {
                                          echo '<option value='.$row['id'].'>'.$row['name'].'</option>';
                                        }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="col-md-12">
                                  <label class="col-md-5 control-label" for="experience_from">Experience From</label>

                                  <div class="col-md-7">
                                    <select class="form-control input-sm" id="experience_from" name="experience_from">
                                      <option value="None">Select Experience From</option>
                                      <?php 
                                        for ($i=0; $i<=30; $i++) {
                                          echo '<option value='.$i.'>'.$i.'</option>';
                                        }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="col-md-12">
                                  <label class="col-md-5 control-label" for="experience_to">Experience To</label>

                                  <div class="col-md-7">
                                    <select class="form-control input-sm" id="experience_to" name="experience_to">
                                      <option value="None">Select Experience To</option>
                                      <?php 
                                        for ($i=0; $i<=30; $i++) {
                                          echo '<option value='.$i.'>'.$i.'</option>';
                                        }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="col-md-12">
                                  <label class="col-md-5 control-label" for="education">Education</label>

                                  <div class="col-md-7">
                                    <select class="form-control input-sm" id="education_id" name="education_id">
                                      <option value="None">Select Education</option>
                                      <?php 
                                        foreach ($educations as $row) {
                                          echo '<option value='.$row['id'].'>'.$row['qualification'].'</option>';
                                        }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="col-md-12">
                                  <label class="col-md-5 control-label" for="salary_currency">Currency</label>

                                  <div class="col-md-7">
                                    <select class="form-control input-sm" id="salary_currency" name="salary_currency">
                                      <option value="None">Select Currency</option>
                                      <?php 
                                        foreach ($currencies as $row) {
                                          echo '<option value='.$row['id'].'>'.$row['currency_name'].'</option>';
                                        }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="col-md-12">
                                  <label class="col-md-5 control-label" for="salary_from">Salary From</label>

                                  <div class="col-md-7">
                                    <select class="form-control input-sm" id="salary_from" name="salary_from">
                                      <option value="None">Select Salary From</option>
                                      <?php 
                                        for ($i=0; $i<=5000000; $i=$i+100000) {
                                          echo '<option value='.$i.'>'.$i.'</option>';
                                        }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="col-md-12">
                                  <label class="col-md-5 control-label" for="salary_to">Salary To</label>

                                  <div class="col-md-7">
                                    <select class="form-control input-sm" id="salary_to" name="salary_to">
                                      <option value="None">Select Salary To</option>
                                      <?php 
                                        for ($i=0; $i<=5000000; $i=$i+100000) {
                                          echo '<option value='.$i.'>'.$i.'</option>';
                                        }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                              </div>
                                

                              <div class="form-group">
                                <div class="col-md-12">
                                  <label class="col-md-5 control-label" for="positions">Total Positions</label>

                                  <div class="col-md-7">
                                    <input class="form-control input-sm" id="positions" name="positions" placeholder="No. of positions" autocomplete="none"/>
                                  </div>
                                </div>
                              </div>

                              <div class="form-group">
                                <div class="col-md-12">
                                  <label class="col-md-12 control-label" for="addPrescreenQuestions" style="text-align: center;">
                                    <div style="cursor: pointer;" onClick="javascript: addPreQuestions();" class="addPreQuestions">Add Pre-screen Questions</div> &nbsp;&nbsp; (optional)
                                    <!--<a href="javascript: addPreQuestions();" class="addPreQuestions">Add Pre-screen Questions</a> &nbsp;&nbsp; (optional)-->
                                  </label>
                                </div>
                              </div>   
                          </div>

                          <div class="form-group">
                          <div class="col-md-12" style="text-align: center;">
                               <button class="btn btn-primary btn-sm" name="tab1_draft" id="tab1_draft" type="button" ><span class="fa fa-save fw-fa"></span> Draft</button>
                               <button class="btn btn-primary btn-sm" name="tab1_next" id="tab1_next" type="button" ><span class="fa fa-save fw-fa"></span> Next</button>
                            <!-- <a href="index.php" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;<strong>Back</strong></a> -->
                          </div>
                        </div>
                  </div>

                  <div class="tab-pane fade <?php echo ($tab == 'jobSettings') ? 'active' : ''; ?>" id="tabs2-pane1">
                      <div class="form-group">
                          <div class="col-md-12">
                            <label class="col-md-6" for="candidate_applications">How many candidate applications do you want to review for this position?</label>

                            <div class="col-md-8">
                              <input class="form-control input-sm" id="total_candidate_applications" name="total_candidate_applications" placeholder="Total candidate applications" autocomplete="none"/>
                              <div>(Min. 50 credits and Max. 200)</div>
                            </div>
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="col-md-12">
                            <label class="col-md-6" for="candidates_interview">How many candidates do you want to interview for this position?</label>

                            <div class="col-md-8">
                              <input class="form-control input-sm" id="total_candidates_interview" name="total_candidates_interview" placeholder="Total candidates interview" autocomplete="none"/>
                            </div>
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="col-md-12">
                            <label class="col-md-6" for="candidates_hire">How many candidates do you want to hire for this position?</label>

                            <div class="col-md-8">
                              <input class="form-control input-sm" id="total_candidates_hire" name="total_candidates_hire" placeholder="Total candidates hire" autocomplete="none"/>
                            </div>
                          </div>
                      </div>


                      <div class="col-md-12">You want to add reward amount or gifts for each stage</div>
                      <div class="col-sm-12">

                        <div class="form-group">
                            <div class="col-md-12">
                              <label class="col-md-12" for="currency_applications">Set reward amount with currecy for each accepted applications</label>

                              <div class="col-md-2">
                                <select class="form-control input-sm" id="currency_applications" name="currency_applications">
                                  <option value="None">Select Currency</option>
                                  <?php
                                      foreach ($currencies as $row) {
                                        echo '<option value='.$row['id'].'>'.$row['currency_name'].'</option>';
                                      }
                                  ?>
                                </select>
                              </div>
                              <div class="col-md-1">
                                <input class="form-control input-sm" id="amount_applications" name="amount_applications" placeholder="Reward amount" autocomplete="none"/>
                              </div>

                              <div class="col-md-3">(Min. ₹1000 and Max. ₹5000)</div>

                              <div class="col-md-2">
                                <select class="form-control input-sm" id="gift_applications" name="gift_applications">
                                  <option value="None">Select Gift</option>
                                  <?php
                                      foreach ($gifts as $row) {
                                        echo '<option value='.$row['id'].'>'.$row['name'].'</option>';
                                      }
                                  ?>
                                </select>
                              </div>

                              <div class="col-md-2">
                                <input class="form-control input-sm" id="gift_applications_amount" name="gift_applications_amount" placeholder="Gift description" autocomplete="none"/>
                              </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                              <label class="col-md-12" for="currency_interview">Set reward amount with currecy for scheduling to Interview</label>

                              <div class="col-md-2">
                                <select class="form-control input-sm" id="currency_interview" name="currency_interview">
                                  <option value="None">Select Currency</option>
                                  <?php
                                      foreach ($currencies as $row) {
                                        echo '<option value='.$row['id'].'>'.$row['currency_name'].'</option>';
                                      }
                                  ?>
                                </select>
                              </div>
                              <div class="col-md-1">
                                <input class="form-control input-sm" id="amount_interview" name="amount_interview" placeholder="Reward amount" autocomplete="none"/>
                              </div>

                              <div class="col-md-3">(Min. ₹1000 and Max. ₹5000)</div>

                              <div class="col-md-2">
                                <select class="form-control input-sm" id="gift_interview" name="gift_interview">
                                  <option value="None">Select Gift</option>
                                  <?php
                                      foreach ($gifts as $row) {
                                        echo '<option value='.$row['id'].'>'.$row['name'].'</option>';
                                      }
                                  ?>
                                </select>
                              </div>

                              <div class="col-md-2">
                                <input class="form-control input-sm" id="gift_interview_amount" name="gift_interview_amount" placeholder="Gift description" autocomplete="none"/>
                              </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                              <label class="col-md-12" for="currency_offerletter">Set reward amount with currecy for offer letter release</label>

                              <div class="col-md-2">
                                <select class="form-control input-sm" id="currency_offerletter" name="currency_offerletter">
                                  <option value="None">Select Currency</option>
                                  <?php
                                      foreach ($currencies as $row) {
                                        echo '<option value='.$row['id'].'>'.$row['currency_name'].'</option>';
                                      }
                                  ?>
                                </select>
                              </div>
                              <div class="col-md-1">
                                <input class="form-control input-sm" id="amount_offerletter" name="amount_offerletter" placeholder="Reward amount" autocomplete="none"/>
                              </div>

                              <div class="col-md-3">(Min. ₹1500 and Max. ₹10000)</div>

                              <div class="col-md-2">
                                <select class="form-control input-sm" id="gift_offerletter" name="gift_offerletter">
                                  <option value="None">Select Gift</option>
                                  <?php
                                      foreach ($gifts as $row) {
                                        echo '<option value='.$row['id'].'>'.$row['name'].'</option>';
                                      }
                                  ?>
                                </select>
                              </div>

                              <div class="col-md-2">
                                <input class="form-control input-sm" id="gift_offerletter_amount" name="gift_offerletter_amount" placeholder="Gift description" autocomplete="none"/>
                              </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                              <label class="col-md-12" for="currency_job">Set reward amount with currecy for joining for each Job</label>

                              <div class="col-md-2">
                                <select class="form-control input-sm" id="currency_job" name="currency_job">
                                  <option value="None">Select Currency</option>
                                  <?php
                                      foreach ($currencies as $row) {
                                        echo '<option value='.$row['id'].'>'.$row['currency_name'].'</option>';
                                      }
                                  ?>
                                </select>
                              </div>
                              <div class="col-md-1">
                                <input class="form-control input-sm" id="amount_job" name="amount_job" placeholder="Reward amount" autocomplete="none"/>
                              </div>

                              <div class="col-md-3">(Min. ₹4000 and Max. ₹20000)</div>

                              <div class="col-md-2">
                                <select class="form-control input-sm" id="gift_job" name="gift_job">
                                  <option value="None">Select Gift</option>
                                  <?php
                                      foreach ($gifts as $row) {
                                        echo '<option value='.$row['id'].'>'.$row['name'].'</option>';
                                      }
                                  ?>
                                </select>
                              </div>

                              <div class="col-md-2">
                                <input class="form-control input-sm" id="gift_job_amount" name="gift_job_amount" placeholder="Gift description" autocomplete="none"/>
                              </div>
                            </div>
                        </div>

                        <div class="form-group">
                          <div class="col-md-12" style="text-align: center;">
                               <button class="btn btn-primary btn-sm" name="tab2_back" id="tab2_back" type="button" ><span class="fa fa-save fw-fa"></span> Back</button>
                               <button class="btn btn-primary btn-sm" name="tab2_draft" id="tab2_draft" type="button" ><span class="fa fa-save fw-fa"></span> Draft</button>
                               <button class="btn btn-primary btn-sm" name="tab2_next" id="tab2_next" type="button" ><span class="fa fa-save fw-fa"></span> Next</button>
                            <!-- <a href="index.php" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;<strong>Back</strong></a> -->
                          </div>
                        </div>

                      </div>
                  </div>

                  <div class="tab-pane fade <?php echo ($tab == 'jobPreview') ? 'active' : ''; ?>" id="tabs3-pane1">
                    <div>Job Preview</div>

                    <div class="col-md-12" style="text-align: center;">
                         <button class="btn btn-primary btn-sm" name="tab3_back" id="tab3_back" type="button" ><span class="fa fa-save fw-fa"></span> Back</button>
                         <button class="btn btn-primary btn-sm" name="tab3_draft" id="tab3_draft" type="button" ><span class="fa fa-save fw-fa"></span> Draft</button>
                         <button class="btn btn-primary btn-sm" name="save" id="tab3_save" type="submit" ><span class="fa fa-save fw-fa"></span> Save</button>
                      <!-- <a href="index.php" class="btn btn-info"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;<strong>Back</strong></a> -->
                    </div>
                  </div>

                       
              </form>
          
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
     <?php include APPPATH.'views/admin/includes/footer.php';?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

   
 
  <script src="<?php echo base_url('assests/vendor/jquery/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assests/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <!-- Core plugin JavaScript-->
    <script src="<?php echo base_url('assests/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>

    <!-- Page level plugin JavaScript-->
    <script src="<?php echo base_url('assests/vendor/chart.js/Chart.min.js'); ?>"></script>
    <script src="<?php echo base_url('assests/vendor/datatables/jquery.dataTables.js'); ?>"></script>
    <script src="<?php echo base_url('assests/vendor/datatables/dataTables.bootstrap4.js'); ?>"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?php echo base_url('assests/js/sb-admin.min.js'); ?>"></script>
    <script src="<?php echo base_url('assests/js/demo/datatables-demo.js'); ?>"></script>
    <script src="<?php echo base_url('assests/js/demo/chart-area-demo.js'); ?>"></script>

  </body>

</html>
