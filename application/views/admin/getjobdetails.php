<!DOCTYPE html>
<html lang="en">

<head>
<title>User Profile</title>
<!-- Bootstrap core CSS-->
<?php echo link_tag('assests/vendor/bootstrap/css/bootstrap.min.css'); ?>
<!-- Custom fonts for this template-->
<?php echo link_tag('assests/vendor/fontawesome-free/css/all.min.css'); ?>
<!-- Page level plugin CSS-->
<?php echo link_tag('assests/vendor/datatables/dataTables.bootstrap4.css'); ?>
<!-- Custom styles for this template-->
<?php echo link_tag('assests/css/sb-admin.css'); ?>

  </head>

  <body id="page-top">

   <?php include APPPATH.'views/admin/includes/header.php';?>

    <div id="wrapper">

      <!-- Sidebar -->
  <?php include APPPATH.'views/admin/includes/sidebar.php';?>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="<?php echo site_url('user/Dashboard'); ?>">User</a>
            </li>
            <li class="breadcrumb-item active">Job Details</li>
          </ol>

          <!-- Page Content -->
          <h3>Job Details</h3>
          <hr>
          <!---- Success Message ---->
            <table border="1" align="center">
              <tr>
                <td><strong>Job ID : </strong></td>
                <td><input type="text" name="job_id" id="job_id" value="<?php echo $jd['id']; ?>" /></td>
              </tr>
              <tr>
                <td><strong>Job Title : </strong></td>
                <td><input type="text" name="job_title" id="job_title" value="<?php echo $jd['title']; ?>" /></td>
              </tr>
              <tr>
                <td><strong>Job Type : </strong></td>
                <td>
                  <select name="job_type" id="job_type">
                  <?php
                    if(count($jd_types) > 0)
                    {
                      foreach($jd_types as $row)
                      {
                        if($jd['job_type'] == $row['name'])
                          $selected = "selected='selected'";
                        else
                          $selected = '';
                        echo "<option value='".$row['id']."' ".$selected.">".$row['name']."</option>";
                      }
                    }
                  ?>
                  </select>
                </td>
              </tr>
              <tr>
                <td><strong>Industry Name : </strong></td>
                <td>
                  <select name="industry_name" id="industry_name">
                  <?php
                    if(count($industries) > 0)
                    {
                      foreach($industries as $row)
                      {
                        if($jd['industry_name'] == $row['name'])
                          $selected = "selected='selected'";
                        else
                          $selected = '';
                        echo "<option value='".$row['id']."' ".$selected.">".$row['name']."</option>";
                      }
                    }
                  ?>
                  </select>
                </td>
              </tr>
              <tr>
                <td><strong>Skills : </strong></td>
                <td><input type="text" name="job_Skills" id="job_Skills" value="<?php echo $jd['skills']; ?>" /></td>
              </tr>
              <tr>
                <td><strong>Company Name : </strong></td>
                <td><input type="text" name="company_name" id="company_name" value="<?php echo $jd['company_name']; ?>" /></td>
              </tr>
              <tr>
                <td><strong>Experience : </strong></td>
                <td>From: 
                  <select name="experience_from" id="experience_from">
                  <?php
                      for($i = 0; $i <= 30; $i++)
                      {
                        if($jd['experience_from'] == $i)
                          $selected = "selected='selected'";
                        else
                          $selected = '';
                        echo "<option value='".$i."' ".$selected.">".$i."</option>";
                      }
                  ?>
                  </select>
                  &nbsp;
                  To: 
                  <select name="experience_to" id="experience_to">
                  <?php
                      for($i = 0; $i <= 30; $i++)
                      {
                        if($jd['experience_to'] == $i)
                          $selected = "selected='selected'";
                        else
                          $selected = '';
                        echo "<option value='".$i."' ".$selected.">".$i."</option>";
                      }
                  ?>
                  </select>
                </td>
              </tr>
              <tr>
                <td><strong>Salary : </strong></td>
                <td>
                  <select name="currency" id="currency">
                  <?php
                    if(count($currencies) > 0)
                    {
                      foreach($currencies as $row)
                      {
                        if($jd['currency'] == $row['currency_name'])
                          $selected = "selected='selected'";
                        else
                          $selected = '';
                        echo "<option value='".$row['id']."' ".$selected.">".$row['currency_name']."</option>";
                      }
                    }
                  ?>
                  </select>
                  &nbsp;
                  Min: 
                  <select name="min_salary" id="min_salary">
                  <?php
                      for($i = 0; $i <= 5000000; $i=$i+100000)
                      {
                        if($jd['min_salary'] == $i)
                          $selected = "selected='selected'";
                        else
                          $selected = '';
                        echo "<option value='".$i."' ".$selected.">".$i."</option>";
                      }
                  ?>
                  </select>
                  &nbsp;
                  Max: 
                  <select name="max_salary" id="max_salary">
                  <?php
                      for($i = 0; $i <= 5000000; $i=$i+100000)
                      {
                        if($jd['max_salary'] == $i)
                          $selected = "selected='selected'";
                        else
                          $selected = '';
                        echo "<option value='".$i."' ".$selected.">".$i."</option>";
                      }
                  ?>
                  </select>
                </td>
              </tr>
              <tr>
                <td><strong>Summary : </strong></td>
                <td><textarea name="job_summary" id="job_summary" cols="80" rows="15"><?php echo $jd['summary']; ?></textarea></td>
              </tr>
              <tr>
                <td><strong>Location : </strong></td>
                <td><input type="text" name="job_location" id="job_location" value="<?php echo $jd['location']; ?>" /></td>
              </tr>
              <tr>
                <td><strong>Total Positions : </strong></td>
                <td><input type="text" name="total_positions" id="total_positions" value="<?php echo $jd['total_positions']; ?>" /></td>
              </tr>
              <tr>
                <td><strong>Candidate Application Credits : </strong></td>
                <td><input type="text" name="candidate_application_credits" id="candidate_application_credits" value="<?php echo $jd['candidate_application_credits']; ?>" /></td>
              </tr>
              <tr>
                <td><strong>Candidate Interview Credits : </strong></td>
                <td><input type="text" name="candidate_interview_credits" id="candidate_interview_credits" value="<?php echo $jd['candidate_interview_credits']; ?>" /></td>
              </tr>
              <tr>
                <td><strong>Candidate Hire Credits : </strong></td>
                <td><input type="text" name="candidate_hire_credits" id="candidate_hire_credits" value="<?php echo $jd['candidate_hire_credits']; ?>" /></td>
              </tr>
              <tr>
                <td><strong>Rewards for Accepted Application : </strong></td>
                <td><input type="text" name="accepted_application_rewards" id="accepted_application_rewards" value="<?php echo $jd['accepted_application_rewards']; ?>" /></td>
              </tr>
              <tr>
                <td><strong>Rewards for Schedule Interview : </strong></td>
                <td><input type="text" name="schedule_interview_rewards" id="schedule_interview_rewards" value="<?php echo $jd['schedule_interview_rewards']; ?>" /></td>
              </tr>
              <tr>
                <td><strong>Rewards for Offerletter Release : </strong></td>
                <td><input type="text" name="offerletter_release_rewards" id="offerletter_release_rewards" value="<?php echo $jd['offerletter_release_rewards']; ?>" /></td>
              </tr>
              <tr>
                <td><strong>Rewards for Joining : </strong></td>
                <td><input type="text" name="joining_rewards" id="joining_rewards" value="<?php echo $jd['joining_rewards']; ?>" /></td>
              </tr>
              <tr>
                <td><strong>Posted on : </strong></td>
                <td><input type="text" name="created_at" id="created_at" value="<?php echo $jd['created_at']; ?>" /></td>
              </tr>

              <tr>
                <td colspan="2" align="center">
                  <input type="submit" class="btn btn-primary" name="update" id="update" value="Update">
                </td>
              </tr>
            </table>
          
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
     <?php include APPPATH.'views/admin/includes/footer.php';?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

   
 
  <script src="<?php echo base_url('assests/vendor/jquery/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assests/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
    <!-- Core plugin JavaScript-->
    <script src="<?php echo base_url('assests/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>

    <!-- Page level plugin JavaScript-->
    <script src="<?php echo base_url('assests/vendor/chart.js/Chart.min.js'); ?>"></script>
    <script src="<?php echo base_url('assests/vendor/datatables/jquery.dataTables.js'); ?>"></script>
    <script src="<?php echo base_url('assests/vendor/datatables/dataTables.bootstrap4.js'); ?>"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?php echo base_url('assests/js/sb-admin.min.js'); ?>"></script>
    <script src="<?php echo base_url('assests/js/demo/datatables-demo.js'); ?>"></script>
    <script src="<?php echo base_url('assests/js/demo/chart-area-demo.js'); ?>"></script>

  </body>

</html>
