<ul class="sidebar navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo site_url('admin/Dashboard'); ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('admin/Manage_Companies'); ?>">
            <i class="fas fa-fw fa-building"></i>
            <span>My Companies</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('admin/Manage_Jobs'); ?>">
            <i class="fas fa-fw fa-suitcase"></i>
            <span>My Jobs</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('admin/Manage_Jobs/addJob'); ?>">
            <i class="fas fa-fw fa-suitcase"></i>
            <span>Create Job</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('admin/Manage_Users'); ?>">
            <i class="fas fa-fw fa-users"></i>
            <span>My Applicants</span>
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" href="<?php echo site_url('admin/Manage_Users'); ?>">
            <i class="fas fa-fw fa-list"></i>
            <span>My Transactions</span></a>
        </li>
 
      </ul>
