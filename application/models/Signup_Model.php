<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class Signup_Model extends CI_Model{

	public function insert($fname,$lname,$emailid,$mnumber,$password,$status){
		$data=array(
					'first_name'=>$fname,
					'last_name'=>$lname,
					'email'=>$emailid,
					'mobile'=>$mnumber,
					'password'=>md5($password),
					//'isActive'=>$status
					'role_id' => 1,
					'city_id' => 1,
				);
		$sql_query=$this->db->insert('users',$data);
		if($sql_query){
			$this->session->set_flashdata('success', 'Registration successfull');
			redirect('user/signup');
		}
		else
		{
			$this->session->set_flashdata('error', 'Somthing went worng. Error!!');
			redirect('user/signup');
		}

	}


}