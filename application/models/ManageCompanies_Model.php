<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

Class ManageCompanies_Model extends CI_Model{
	
	public function getcompanydetails($cid = '') {

      	$query = $this->db->query("SELECT c.id, c.company_name, c.company_address, c.company_contact, c.created_at FROM companies as c");

      	$i = 0;

		foreach ($query->result() as $row)
		{
			$result[$i]['id'] = $row->id;
	        $result[$i]['company_name'] = $row->company_name;
	        $result[$i]['company_address'] = $row->company_address;
	        $result[$i]['company_contact'] = $row->company_contact;
	        $result[$i++]['created_at'] = $row->created_at;
	        //$result[$i++]['statusName'] = $row->statusName;
		}

		return $result;
	}

	public function deletecompany($cid){
		$sql_query=$this->db->where('id', $cid)
	                ->delete('companies');
	}

}