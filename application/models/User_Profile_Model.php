<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User_Profile_Model extends CI_Model {

    public function getprofile($userid) {
    	$query = $this->db->select('id, first_name, last_name, email, mobile')
                    ->where('id', $userid)
                    ->from('users')
                    ->get();
      return $query->row();
    }

    public function update_profile($fname, $lname, $mnumber, $userid) {
      $data = array(
               'firstName' =>$fname,
               'lastName' => $lname,
               'mobileNumber' => $mnumber
              );

      $sql_query = $this->db->where('id', $userid)->update('users', $data); 
    }

}