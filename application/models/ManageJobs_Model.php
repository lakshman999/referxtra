<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

Class ManageJobs_Model extends CI_Model{
	
	public function getjobdetails($userType = '') {

		$where = '';
	  	if(!empty($_POST)) {
	  		$parameters = $_POST;

	  		foreach($parameters as $key => $value) {
	  			if(!empty($value)) {
	  				if($key == 'location')
	  					$where .= ' AND city_id = '.$value;
	  				else
	  					$where .= ' AND j.'.$key.' LIKE "%'.$value.'%"';
	  			}
	  		}
	  	} else if(!empty($this->uri->segment(3))) {

	  		$where = ' AND j.id = ' . $this->uri->segment(4);

	  	} else if(!empty($userType)) {

	  		$where = ' AND j.created_by = ' . $userType;

	  	}


	  	
      	$query = $this->db->query("SELECT j.id, j.title, jt.name as job_type, i.name as industry_name, j.skills, com.company_name, j.experience_from, j.experience_to, j.min_salary, j.max_salary, j.summary, CONCAT(c.name, ', ', s.name, ', ', co.name) AS location, j.created_at, j.total_positions, j.candidate_application_credits, j.candidate_interview_credits, j.candidate_hire_credits, j.accepted_application_rewards, j.schedule_interview_rewards, j.offerletter_release_rewards, j.joining_rewards, st.name as status, co.currency_name, (accepted_application_rewards + schedule_interview_rewards + offerletter_release_rewards + joining_rewards) AS total_rewards
						FROM `jobs` j, `cities` c, `job_types` jt, countries co, states s, status st, companies com, industries i
						WHERE j.city_id = c.id AND j.job_type_id = jt.id AND co.id = s.country_id AND c.id = j.city_id AND s.id = c.state_id AND co.id = s.country_id AND j.status_id = st.id AND j.company_id = com.id AND j.industry_id = i.id ".$where." ORDER BY j.created_at DESC;");

      	//echo $this->db->last_query(); die;

      	
      	$result = array();

      	if($query->num_rows() > 0)
      	{
      		$i = 0;
			foreach ($query->result() as $row)
			{
				$result[$i]['id'] = $row->id;
				$result[$i]['title'] = $row->title;
				$result[$i]['job_type'] = $row->job_type;
				$result[$i]['industry_name'] = $row->industry_name;
				$result[$i]['skills'] = $row->skills;
		        $result[$i]['company_name'] = $row->company_name;
		        $result[$i]['experience_from'] = $row->experience_from;
		        $result[$i]['experience_to'] = $row->experience_to;
		        $result[$i]['min_salary'] = $row->min_salary;
		        $result[$i]['max_salary'] = $row->max_salary;
		        $result[$i]['summary'] = $row->summary;
		        $result[$i]['location'] = $row->location;
		        $result[$i]['total_positions'] = $row->total_positions;

		        $result[$i]['total_rewards'] = $row->total_rewards;
		        $result[$i]['candidate_application_credits'] = $row->candidate_application_credits;
		        $result[$i]['candidate_interview_credits'] = $row->candidate_interview_credits;
		        $result[$i]['candidate_hire_credits'] = $row->candidate_hire_credits;
		        $result[$i]['accepted_application_rewards'] = $row->accepted_application_rewards;
		        $result[$i]['schedule_interview_rewards'] = $row->schedule_interview_rewards;
		        $result[$i]['offerletter_release_rewards'] = $row->offerletter_release_rewards;
		        $result[$i]['joining_rewards'] = $row->joining_rewards;
		        
		        $result[$i]['status'] = $row->status;
		        $result[$i]['created_at'] = $row->created_at;
		        $result[$i++]['currency'] = $row->currency_name;
			}
		}

		return $result;
	}

	public function deletecompany($cid){
		$sql_query=$this->db->where('id', $cid)
	                ->delete('companies');
	}

	public function getJobtypes() {
		$query = $this->db->query("select id, name FROM job_types");

		$result = array();
		
		if($query->num_rows() > 0)
      	{
      		$i = 0;
			foreach ($query->result() as $row)
			{
				$result[$i]['id'] = $row->id;
				$result[$i++]['name'] = $row->name;
			}
		}

		return $result;
	}

	public function getIndustries() {
		$query = $this->db->query("select id, name FROM industries");

		$result = array();
		
		if($query->num_rows() > 0)
      	{
      		$i = 0;
			foreach ($query->result() as $row)
			{
				$result[$i]['id'] = $row->id;
				$result[$i++]['name'] = $row->name;
			}
		}

		return $result;
	}

	public function getCurrencies() {
		$query = $this->db->query("select id, currency_name FROM countries where currency_name != ''");

		$result = array();
		
		if($query->num_rows() > 0)
      	{
      		$i = 0;
			foreach ($query->result() as $row)
			{
				$result[$i]['id'] = $row->id;
				$result[$i++]['currency_name'] = $row->currency_name;
			}
		}

		return $result;
	}

	public function getLocations() {
		$query = $this->db->query("SELECT c.id, CONCAT(c.name, ', ', s.name, ', ', co.name) AS location FROM countries co JOIN states s ON co.id = s.country_id JOIN cities c ON s.id = c.state_id ORDER BY co.name LIMIT 10;");
	    
	    if($query->num_rows() > 0)
      	{
      		$i = 0;
			foreach ($query->result() as $row)
			{
				$result[$i]['id'] = $row->id;
				$result[$i++]['location'] = $row->location;
			}
		}

        return $result;
	}

	public function getCompanies() {
		$query = $this->db->query("SELECT id, company_name FROM companies ORDER BY company_name;");
	    
	    if($query->num_rows() > 0)
      	{
      		$i = 0;
			foreach ($query->result() as $row)
			{
				$result[$i]['id'] = $row->id;
				$result[$i++]['company_name'] = $row->company_name;
			}
		}

        return $result;
	}

	public function getEducations() {
		$query = $this->db->query("SELECT id, qualification FROM education ORDER BY qualification;");
	    
	    if($query->num_rows() > 0)
      	{
      		$i = 0;
			foreach ($query->result() as $row)
			{
				$result[$i]['id'] = $row->id;
				$result[$i++]['qualification'] = $row->qualification;
			}
		}

        return $result;
	}

	public function getGifts() {
		$query = $this->db->query("SELECT id, name FROM reward_gifts WHERE status_id = 1 ORDER BY name;");
	    
	    if($query->num_rows() > 0)
      	{
      		$i = 0;
			foreach ($query->result() as $row)
			{
				$result[$i]['id'] = $row->id;
				$result[$i++]['name'] = $row->name;
			}
		}

        return $result;
	}

}