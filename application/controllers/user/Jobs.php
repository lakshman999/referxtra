<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class Jobs extends CI_Controller {


		public function filterJobs(){
			
			if($this->input->post('refer_job')) {
				$refer_job_title = $this->input->post('refer_job_title');
				$refer_companyname = $this->input->post('refer_companyname');
				$refer_location = $this->input->post('refer_location');
				$this->load->model('Jobs_Model');
				$searchJobs = $this->Jobs_Model->getReferJobs($refer_job_title, $refer_companyname, $refer_location);
			} elseif($this->input->post('search_job')) {
				$search_job_title = $this->input->post('search_job_title');
				$search_location = $this->input->post('search_location');

				$this->load->model('Jobs_Model');
				$searchJobs = $this->Jobs_Model->getSearchJobs($search_job_title, $search_location);

				$this->load->view('user/getsearchjobs',['searchJobs' => $searchJobs]);

			} /*elseif($this->input->post('search_job'))
				$emailid=$this->input->post('search_job_title');
				$password=$this->input->post('search_location');
			}*/



			/*$this->form_validation->set_rules('emailid','Email id','required|valid_email');
			$this->form_validation->set_rules('password','Password','required');
			if($this->form_validation->run()){
				$emailid=$this->input->post('emailid');
				$password=$this->input->post('password');
				$status=1;	
				$this->load->model('User_Login_Model');
				$validate=$this->User_Login_Model->validatelogin($emailid,$password,$status);
				if($validate)
				{
					$this->session->set_userdata('uid',$validate);
					return redirect('user/dashboard');
				} else{
					$this->session->set_flashdata('error', 'Invalid details. Please try again with valid details');
					redirect('user/login');
				}

			} else {
				$this->load->view('user/login');
			}*/
		}

		//function for displaying candidate jobs
		public function candidate_jobs() {

			/*$this->session->unset_userdata('uid');
			$this->session->sess_destroy();
			return redirect('user/login');*/
		}

		public function myjobs() {
			$user_type = $this->input->post('user_type');
			
			if(!empty($this->input->post('user_type')))
				$this->session->set_userdata('user_type',$user_type);

			$this->load->model('Jobs_Model');
			$result = $this->Jobs_Model->getSearchJobs('', '', $user_type);

			$this->load->view('user/getuserjobs',['result' => $result, 'user_type' => $user_type]);

		}

		public function createjob() {

			$this->load->model('ManageJobs_Model');
			$locations = $this->ManageJobs_Model->getLocations();
			$jobtypes = $this->ManageJobs_Model->getJobtypes();
			$companies = $this->ManageJobs_Model->getCompanies();
			$educations = $this->ManageJobs_Model->getEducations();
			$currencies = $this->ManageJobs_Model->getCurrencies();
			$gifts = $this->ManageJobs_Model->getGifts();
			$industries = $this->ManageJobs_Model->getIndustries();

			$this->load->view('user/create_job', ['locations' => $locations, 'jobtypes' => $jobtypes, 'companies' => $companies, 'educations' => $educations, 'currencies' => $currencies, 'gifts' => $gifts, 'industries' => $industries]);
		}

		public function savePreScreenQuestions() {

			//print_r($this->input->post('question[]'));

			$questions = $this->input->post('question[]');

			foreach($questions as $question) {
				$preQuestions[]['description'] = $question;
			}

			$this->load->model('Jobs_Model');

			$result = $this->Jobs_Model->savePreScreenQuestions($preQuestions);

			return $result;
		}

}