<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class Manage_Companies extends CI_Controller {
	function __construct(){
		parent::__construct();
		if(! $this->session->userdata('adid'))
			redirect('admin/login');
	}

	public function index(){
		$this->load->model('ManageCompanies_Model');
		$companies = $this->ManageCompanies_Model->getcompanydetails();
		$this->load->view('admin/manage_companies',['companydetails' => $companies]);
	}

	// For particular Record
	public function getcompanydetail($uid)
	{
		$this->load->model('ManageCompanies_Model');
		$cdetail=$this->ManageCompanies_Model->getcompanydetail($uid);
		$this->load->view('admin/getcompanydetails',['cd'=>$cdetail]);
	}

	public function deletecompany($uid)
	{
		$this->load->model('ManageCompanies_Model');
		$this->ManageCompanies_Model->deleteuser($uid);
		$this->session->set_flashdata('success', 'Company data deleted');
		redirect('admin/manage_companies');
	}


}