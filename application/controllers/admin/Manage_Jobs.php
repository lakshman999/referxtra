<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class Manage_Jobs extends CI_Controller {
	function __construct(){
		parent::__construct();
		if(! $this->session->userdata('adid'))
			redirect('admin/login');
	}

	public function index(){
		$this->load->model('ManageJobs_Model');
		$jobs = $this->ManageJobs_Model->getjobdetails();
		$this->load->view('admin/manage_jobs',['jobdetails' => $jobs]);
	}

	// For particular Record
	public function getjobdetail($jid = '')
	{
		$this->load->model('ManageJobs_Model');
		$jdetail = $this->ManageJobs_Model->getjobdetails($jid);
		$jobtypes = $this->ManageJobs_Model->getjobtypes();
		$industries = $this->ManageJobs_Model->getindustries();
		$currencies = $this->ManageJobs_Model->getcurrencies();
		$this->load->view('admin/getjobdetails',['jd' => $jdetail[0], 'jd_types' => $jobtypes, 'industries' => $industries, 'currencies' => $currencies]);
	}

	public function addJob() {

		$this->load->model('ManageJobs_Model');
		$locations = $this->ManageJobs_Model->getLocations();
		$jobtypes = $this->ManageJobs_Model->getJobtypes();
		$companies = $this->ManageJobs_Model->getCompanies();
		$educations = $this->ManageJobs_Model->getEducations();
		$currencies = $this->ManageJobs_Model->getCurrencies();
		$gifts = $this->ManageJobs_Model->getGifts();
		$industries = $this->ManageJobs_Model->getIndustries();

		$this->load->view('admin/create_job', ['locations' => $locations, 'jobtypes' => $jobtypes, 'companies' => $companies, 'educations' => $educations, 'currencies' => $currencies, 'gifts' => $gifts, 'industries' => $industries]);
	}

	public function createJob() {
		$createJob = array(
			'id' => $this->input->post('jobid'),
			'title' => $this->input->post('job_title'),
			'city_id' => $this->input->post('location'),
			'job_type_id' => $this->input->post('job_type'),
			'summary' => $this->input->post('summary'),
			'skills' => $this->input->post('skills'),
			'company_id' => $this->input->post('company_id'),
			'industry_id' => $this->input->post('industry_id'),
			'experience_from' => $this->input->post('experience_from'),
			'experience_to' => $this->input->post('experience_to'),
			'education_id' => $this->input->post('education_id'),
			'currency' => $this->input->post('salary_currency'),
			'min_salary' => $this->input->post('salary_from'),
			'max_salary' => $this->input->post('salary_to'),
			'total_positions' => $this->input->post('positions'),
			'candidate_application_credits' => $this->input->post('total_candidate_applications'),
			'candidate_interview_credits' => $this->input->post('total_candidates_interview'),
			'candidate_hire_credits' => $this->input->post('total_candidates_hire'),
			'currency_applications' => $this->input->post('currency_applications'),
			'accepted_application_rewards' => $this->input->post('amount_applications'),
			'gift_applications' => $this->input->post('gift_applications'),
			'gift_applications_details' => $this->input->post('gift_applications_details'),
			'currency_interview' => $this->input->post('currency_interview'),
			'schedule_interview_rewards' => $this->input->post('amount_interview'),
			'gift_interview' => $this->input->post('gift_interview'),
			'gift_interview_details' => $this->input->post('gift_interview_amount'),
			'currency_offerletter' => $this->input->post('currency_offerletter'),
			'offerletter_release_rewards' => $this->input->post('amount_offerletter'),
			'gift_offerletter' => $this->input->post('gift_offerletter'),
			'gift_offerletter_details' => $this->input->post('gift_offerletter_amount'),
			'currency_joining' => $this->input->post('currency_job'),
			'joining_rewards' => $this->input->post('amount_job'),
			'gift_joining' => $this->input->post('gift_job'),
			'gift_joining_details' => $this->input->post('gift_job_amount')
		);
		
		$this->load->model('ManageJobs_Model');
		$sql_query = $this->db->insert('jobs',$createJob);
		if($sql_query){
			$this->session->set_flashdata('success', 'Registration successfull');
			redirect('admin/manage_jobs');
		}
	}

	public function deletejob($jid)
	{
		$this->load->model('ManageJobs_Model');
		$this->ManageJobs_Model->deletejob($uid);
		$this->session->set_flashdata('success', 'Job data deleted');
		redirect('admin/manage_jobs');
	}


}